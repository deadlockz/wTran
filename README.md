# wTran - Words offline Translator and Thesaurus

I try to build a Desktop version with nodejs and electron. You find old app and old executable
in my dropbox: [Download](https://www.dropbox.com/sh/u18mceddc5u0008/AAD7rxHHM9SdnO_v2iWBdroLa?dl=0)

If you want to run the code with plain [node.js](https://nodejs.org)
you have to:

- install nodejs (and npm)
- do `npm i -D electron@latest`
- do `npm start`

If you want to build your own desktop executable:

- install [yarn](https://yarnpkg.com)
- do `npm install`
- do `yarn add electron-builder --dev`
- do `yarn dist`

## Screenshot

Nice small window with tray icon. Type `:q` + enter into search filed to close the window.

![tray icon](screenshot.png)

## License - OpenThesaurus File

My Code uses a text file from [openthesaurus.de](https://www.openthesaurus.de)
with this license:

    OpenThesaurus - German Thesaurus in text format
    Automatically generated 2018-02-01 23:01
    https://www.openthesaurus.de
    Copyright (C) 2003-2017 Daniel Naber (naber at danielnaber de)
    
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.
    
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
